package com.example.demo.service;

import com.example.demo.dto.PriceSummaryDto;
import com.example.demo.dto.ProductDto;
import com.example.demo.dto.QuantityDto;
import com.example.demo.dto.QuantityPriceDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.model.ShoppingCart;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ShoppingCartService {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    public boolean addProduct(String productId, QuantityDto quantityDto, String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        System.out.println("Am gasit shopping cart-ul cu id-ul: " + shoppingCart.getShoppingCartId());
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        if (optionalProduct.isEmpty()) {
            System.out.println("optional product is empty");
            return false;
        }
        Product product = optionalProduct.get();
        Integer quantity = Integer.valueOf(quantityDto.getQuantity());
        if (product.getStock().getQuantity() < quantity) {
            System.out.println("product is out of stock");
            return false;
        }
        for (int i = 0; i < quantity; i++) {
            shoppingCart.addProduct(product);
        }
        shoppingCartRepository.save(shoppingCart);
        return true;
    }

    public Map<ProductDto, QuantityPriceDto> retrieveShoppingCartContent(String userEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(userEmail);
        List<Product> products = shoppingCart.getProducts();
        Map<ProductDto, QuantityPriceDto> result = new LinkedHashMap<>();
        for (Product product : products) {
            ProductDto productDto = productMapper.map(product);
            if (result.containsKey(productDto)){
                QuantityPriceDto quantityPriceDto = result.get(productDto);

                Integer numberOfProducts = quantityPriceDto.getQuantity();
                quantityPriceDto.setQuantity(numberOfProducts + 1);

                Integer totalPrice = quantityPriceDto.getTotalPrice();
                quantityPriceDto.setTotalPrice(totalPrice + Integer.parseInt(productDto.getPrice()));
            }else{
                QuantityPriceDto quantityPriceDto = new QuantityPriceDto();
                quantityPriceDto.setTotalPrice(Integer.valueOf(productDto.getPrice()));
                quantityPriceDto.setQuantity(1);

                result.put(productDto, quantityPriceDto);
            }
        }
        System.out.println(result);
        return result;
    }

    public PriceSummaryDto computePriceSummaryDto(Map<ProductDto, QuantityPriceDto> productMap) {
        int subtotalPrice = productMap.values().stream()
                .map(QuantityPriceDto::getTotalPrice)
                .mapToInt(Integer::intValue)
                .sum();

        int shipping = 50;
        int total = subtotalPrice + shipping;

        PriceSummaryDto priceSummaryDto = new PriceSummaryDto(subtotalPrice, shipping, total);
        return priceSummaryDto;
    }
}

