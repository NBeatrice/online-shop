package com.example.demo.model.enums;

public enum Category {
    ELECTRONICS, TOYS, BOOKS, CARS
}
