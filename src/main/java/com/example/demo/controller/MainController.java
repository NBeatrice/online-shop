package com.example.demo.controller;

import com.example.demo.dto.*;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;
import com.example.demo.service.ShoppingCartService;
import com.example.demo.service.UserService;
import com.example.demo.validator.ProductDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDtoValidator productDtoValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private OrderService orderService;

    @GetMapping("/addProduct")
    public String productPageGet(Model model,@ModelAttribute("addProductMessage") String addProductMessage) {
        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        model.addAttribute("addProductMessage", addProductMessage);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String productPagePost(@ModelAttribute(name="productDto") ProductDto productDto,
                                  BindingResult bindingResult, RedirectAttributes redirectAttributes,@RequestParam(value = "productImage", required = false) MultipartFile multipartFile){
        productDtoValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()){
            System.out.println("There is an error");
            return "addProduct";
        }
        productService.addProduct(productDto, multipartFile);
        redirectAttributes.addFlashAttribute("addProductMessage", "The product was added successfully ");
        return"redirect:/addProduct";
    }
    @GetMapping("/home")
    public String homePageGet(Model model){
        List<ProductDto> productDtoList = productService.getAllAvailableProductDtos();
        model.addAttribute("productDtoList", productDtoList);
        return "home";
    }

    @GetMapping("/product/{productId}") // asteptam un request pe produsul cutare cu id-ul cutare
    public String viewProductGet(@PathVariable(value = "productId") String productId, Model model){
        Optional<ProductDto> optionalProductDto = productService.findProductDtoById(productId);
        if(optionalProductDto.isEmpty()){
            return "error";
        }
        model.addAttribute("productDto", optionalProductDto.get());
        model.addAttribute("quantityDto", new QuantityDto());
        return "viewProduct";
    }

    @GetMapping("/registration")
    public String registrationGet(Model model){
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        return "registration";
    }

    @GetMapping("/login")
    public String loginGet() {
        return "login";
    }

    @PostMapping(value = "/registration")
    public String registrationPost(@ModelAttribute(name="userDto") UserDto userDto){
        userService.registerUser(userDto);
        return "redirect:/login";
    }

    @PostMapping(value = "/product/{productId}/add")
    public String addToCartPost(@PathVariable(value = "productId") String productId,
                                @ModelAttribute(name="quantityDto") QuantityDto quantityDto,
                                Authentication authentication){
        System.out.println("Vreau sa adaug in cart produsul cu product id: " + productId
                + " si cantitatea: " + quantityDto.getQuantity() + " in cosul user-ului: " +
                authentication.getName());
        shoppingCartService.addProduct(productId, quantityDto, authentication.getName());
        return "redirect:/product/" + productId;
    }

    @GetMapping(value = "/buyNow")
    public String buyNowGet(Authentication authentication, Model model){
        Map<ProductDto, QuantityPriceDto> productMap = shoppingCartService.retrieveShoppingCartContent(authentication.getName());
        model.addAttribute("productMap",productMap);

        PriceSummaryDto priceSummaryDto = shoppingCartService.computePriceSummaryDto(productMap);
        model.addAttribute("priceSummaryDto", priceSummaryDto);
        return "buyNow";
    }

    @GetMapping(value = "/confirmation")
    public String confirmationGet(Authentication authentication){
        orderService.placeOrder(authentication.getName());
        return "confirmation";
    }

    @GetMapping(value = "/orders")
    public String ordersGet(Authentication authentication, Model model){
        List<OrderDto> orders = orderService.createOrderDtoList(authentication.getName());
        model.addAttribute("orders", orders);
        return "orders";
    }
}
