package com.example.demo.model;

import com.example.demo.model.enums.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String email;
    private String password;
    @Enumerated(value = EnumType.STRING)
    private Role role;
    private String phoneNumber;
    private String address;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Wishlist wishlist;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private ShoppingCart shoppingCart;
    @OneToMany(mappedBy = "user")
    private List<Order> orders;
}
