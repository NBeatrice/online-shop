package com.example.demo.model;

import com.example.demo.model.enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue
    private Integer orderId;
    @ManyToOne
    @JoinColumn
    private User user;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "order_product", joinColumns = @JoinColumn(name= "orderId"),inverseJoinColumns = @JoinColumn(name = "product"))
    private List<Product> products;
    @Enumerated(value = EnumType.STRING)
    private OrderStatus orderStatus;
    private LocalDateTime dateTime;

}
